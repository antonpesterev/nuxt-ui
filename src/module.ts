import {
  defineNuxtModule,
  addPlugin,
  createResolver,
  addComponent,
} from "@nuxt/kit";
import { resolve } from "path";

// Module options TypeScript interface definition
export interface ModuleOptions {}

export default defineNuxtModule<ModuleOptions>({
  meta: {
    name: "nuxt-ui",
    configKey: "nuxt-ui",
  },
  // Default configuration options of the Nuxt module
  defaults: {},
  setup(options, nuxt) {
    const resolver = createResolver(import.meta.url);
    const runtimeDir = resolver.resolve("./runtime");
    const componentsDir = resolver.resolve("./runtime/components");

    nuxt.options.build.transpile.push(runtimeDir);

    addComponent({
      name: "MyUiButton",
      filePath: resolve(componentsDir, "MyUiButton.vue"),
    });

    // Do not add the extension since the `.ts` will be transpiled to `.mjs` after `npm run prepack`
    // addPlugin(resolver.resolve('./runtime/plugin'))
  },
});
